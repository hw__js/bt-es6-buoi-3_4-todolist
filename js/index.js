import {
  letContentFromInput,
  renderCompleteTask,
  renderTask,
  resetTask,
  sort_AtoZ,
  sort_ZtoA,
  syncState,
} from "./controllers/taskController.js";
import { Task } from "./model/taskModel.js";

let myTasks = [];
const TASK = "state";

let dataJson = localStorage.getItem(TASK);
if (dataJson) {
  let rawData = JSON.parse(dataJson);

  myTasks = rawData.map(function (item) {
    return new Task(item.status, item.id, item.content);
  });

  // Sort array from A to Z of new tags
  sort_AtoZ(myTasks);
  renderTask(myTasks);
}

document.getElementById("addItem").onclick = function () {
  let newTask = letContentFromInput();
  myTasks.push(newTask);

  syncState(myTasks);

  sort_AtoZ(myTasks);

  renderTask(myTasks);
  console.log("myTasks: ", myTasks);
  resetTask();
};

deleteTaskOnToDo();

completeTask();

function deleteTaskOnToDo() {
  // get all "li" tags
  let taskList = document.querySelectorAll("#todo > li");

  // loop each "li" tag in "li" list
  taskList.forEach((eachTask) => {
    console.log("eachTask: ", eachTask);

    let removeClick = eachTask.querySelector(".buttons > .remove");
    removeClick.addEventListener("click", () => {
      let needObject = myTasks.find((item) => item.id == eachTask.id);
      console.log("needObject: ", needObject);

      // let index = myTasks.findIndex((item) => item.id == eachTask.id);
      // delete myTasks[index];

      if (needObject) {
        myTasks = myTasks.filter((removeItem) => removeItem !== needObject);
      }

      renderTask(myTasks);
      deleteTaskOnToDo();
    });
  });
}

function deleteTaskOnComplete(data) {
  // get all "li" tags
  let completeList = document.querySelectorAll("#completed > li");

  // loop each "li" tag in "li" list
  completeList.forEach((eachTask) => {
    console.log("eachTask: ", eachTask);

    let removeClickInComplete = eachTask.querySelector(".buttons > .remove");
    removeClickInComplete.addEventListener("click", () => {
      let needObject = data.find((item) => item.id == eachTask.id);
      console.log("needObject: ", needObject);

      // let index = myTasks.findIndex((item) => item.id == eachTask.id);
      // delete myTasks[index];

      if (needObject) {
        data = data.filter((removeItem) => removeItem !== needObject);
      }

      renderCompleteTask(data);
      deleteTaskOnComplete(data);
    });
  });
}

function completeTask() {
  let completeArr = [];

  // get all "li" tags
  let taskList = document.querySelectorAll("#todo > li");
  // loop each "li" tag in "li" list
  taskList.forEach((eachTask) => {
    // click complete check to show the object which is need
    eachTask
      .querySelector(".buttons > .complete")
      .addEventListener("click", () => {
        console.log("eachTask: ", eachTask);

        eachTask.style.display = "none";

        // Look for object when user click
        let needObject = myTasks.find((item) => item.id == eachTask.id);
        console.log("needObject: ", needObject);

        if (needObject) {
          myTasks = myTasks.filter((removeItem) => removeItem !== needObject);

          completeArr.push(needObject);
        }
        // console.log("completeArr: ", completeArr);
        console.log("myTasks: ", myTasks);

        // Sort array from Z to A of complete tags
        sort_ZtoA(completeArr);

        renderCompleteTask(completeArr);
        deleteTaskOnComplete(completeArr);

        //===============================================================
        //===============================================================

        let list = document.querySelectorAll("#completed > li");
        // loop each "li" tag in "li" list
        list.forEach((task) => {
          // click complete check to show the object which is need
          task
            .querySelector(".buttons > .complete")
            .addEventListener("click", () => {
              console.log("task: ", task);

              task.style.display = "none";
              let object = completeArr.find((item) => item.id == task.id);

              myTasks.push(object);

              sort_AtoZ(myTasks);
              renderTask(myTasks);
              completeTask();
              deleteTaskOnToDo();
            });
        });
      });
  });
}
