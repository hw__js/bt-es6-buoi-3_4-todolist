import { Task } from "../model/taskModel.js";

export function letContentFromInput() {
  let content = document.getElementById("newTask").value.trim();
  let id = generateID();
  let status = "new";

  return new Task(status, id, content);
}

export function generateID() {
  var randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
  return randLetter + Date.now();
}

export function syncState(state) {
  localStorage.setItem("state", JSON.stringify(state));
}

export function resetState() {
  localStorage.setItem("state", null);
}

export function getState() {
  return JSON.parse(localStorage.getItem("state"));
}

export function renderTask(list) {
  let contentHTML = "";
  list.forEach((element) => {
    contentHTML += `
        <li id="${element.id}">
            <p>${element.content}</p>

            <div class="buttons">
                <i class="fa fa-trash-alt remove"></i>
                <span class="complete" id="completeBtn">
                    <i class="fa fa-check-circle"  value="${element.status}"></i>
                </span>

            </div>

        </li>`;
  });
  document.getElementById("todo").innerHTML = contentHTML;
}

export function renderCompleteTask(list) {
  let contentComleteHTML = "";
  list.forEach((element) => {
    contentComleteHTML += `
        <li id="${element.id}">
            <p>${element.content}</p>
    
            <div class="buttons">
                <i class="fa fa-trash-alt remove"></i>
                <span class="complete" id="completeBtn">
                    <i class="fa fa-check-circle" value="${element.status}" ></i>
                </span>
            </div>
    
        </li>`;
  });
  document.getElementById("completed").innerHTML = contentComleteHTML;
}

export function resetTask() {
  document.getElementById("newTask").value = "";
}

export let sort_AtoZ = (data) => {
  data.sort(function (a, b) {
    if (a.content < b.content) {
      return -1;
    }
    if (a.content > b.content) {
      return 1;
    }
    return 0;
  });
};

export let sort_ZtoA = (data) => {
  data.sort(function (a, b) {
    if (a.content < b.content) {
      return 1;
    }
    if (a.content > b.content) {
      return -1;
    }
    return 0;
  });
};
